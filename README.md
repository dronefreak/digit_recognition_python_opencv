# README #

Digit Recognition algorithm using the basic kNN classification. The following image ![](https://bytebucket.org/dronefreak/digit_recognition_python_opencv/raw/0724445d240cea324056b4f3709d1bf56f167a77/digits.png)

is used to train the classifier for digit recognition.  

The image is divided in 5000 parts of 20x20 each. Each section contains a digit in a different style.

### Setup ###

Clone the repo and run the `OCR.py` and give your input image as test.


### Contribution guidelines ###

* Writing tests
* Code review
* use this repo with another involving [license plate detection](https://bitbucket.org/dronefreak/python_opencv_licence_plate_detection)